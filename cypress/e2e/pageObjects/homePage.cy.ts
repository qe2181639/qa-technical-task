export const linkCount = async () => {
    try{
        cy.get('a').each((link) => {
            cy.request(link.prop('href')).then((response) => {
              expect(response.status).to.equal(200)
            })
          })
    }catch{
        throw new Error("Not all links are successful"); 
    }
};

export const tableRowCount = async () => {
    const tableCount = 2;
    try{
        cy.get('table').find('tr').its('length').then((rowCount) => {
            cy.log(`Number of rows in the table: ${rowCount}`)
            expect(rowCount).to.equal(tableCount)
          })
    }catch{
        throw new Error("The expected and actual table count does not align")

    }
};