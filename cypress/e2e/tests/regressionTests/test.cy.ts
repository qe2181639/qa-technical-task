/// <reference types="cypress" />

import { linkCount, tableRowCount } from "../../pageObjects/homePage.cy";




describe('Automation tests', () => {
  beforeEach(() => {
    cy.visit(Cypress.env('NEXT_PUBLIC_CYPRESS_URL'));
  })

  it('Should verify all links on the page', () => {
    linkCount();
  })

  it('Should count the number of table rows', () => {
   tableRowCount()
  })
})

 
 
