Feature: Checking if four points form a square

  Scenario: Successful square formation
    Given the user is on the square checker page
    When the user fills in valid coordinates for a square: "1,1", "1,2", "2,1", "2,2"
    And the user clicks the "Check" button
    Then the user should see a success message confirming a square is formed

  Scenario: Failing to form a square due to missing coordinates
    Given the user is on the square checker page
    When the user fills in coordinates for a square, missing one or more coordinates
    And the user submits the form
    Then the user should see an error message "This field is required" indicating missing coordinates

  Scenario: Failing to form a square due to non-square coordinates
    Given the user is on the square checker page
    When the user fills in coordinates that do not form a square: "1,1", "1,2", "3,3", "4,4"
    And the user clicks the "Check" button
    Then the user should see an error message indicating that the coordinates do not form a square

  Scenario: Failing to form a square due to invalid coordinates
    Given the user is on the square checker page
    When the user fills in invalid coordinates: "1,1", "1,a", "2,3", "3,4"
    And the user clicks the "Check" button
    Then the user should see an error message indicating invalid coordinates

  Scenario: Clearing the coordinates
    Given the user is on the square checker page
    When the user fills in coordinates in the text boxes
    And the user clicks the "Clear" button
    Then all coordinate text boxes should be empty

  Scenario: Obtain the new page similar to mockup
    Given the user is on the square checker page
    When the user fills in coordinates in the text boxes
    Then a new page should be obtained similar to the mockup provided

