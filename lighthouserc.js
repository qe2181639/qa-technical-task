module.exports = {
	ci: {
		collect: {
			numberOfRuns: 2,
			startServerCommand: 'npm start',
			url: process.env.URL,
			settings: {
				onlyCategories: [
					'performance',
					'accessibility',
					'best-practices',
					'seo',
				],
				skipAudits: ['uses-http2'],
				formFactor: 'desktop',
				screenEmulation: {
					mobile: false,
				},
				chromeFlags: '--no-sandbox',

				extraHeaders: JSON.stringify({
					Cookie: 'customCookie=1;foo=bar',
					Authorization:
						'Basic d2hpdGVsYWJlbFVzZXI6dmFsbGV5ODFncmVlbjUxdHJhaW4yNg==',
				}),
			},
		},
		assert: {
			assertions: {
				'categories:performance': [
					'warn',
					{ minScore: 0.9, aggregationMethod: 'median-run' },
				],
				'categories:accessibility': [
					'warn',
					{ minScore: 1, aggregationMethod: 'pessimistic' },
				],
				'categories:best-practices': [
					'warn',
					{ minScore: 1, aggregationMethod: 'pessimistic' },
				],
				'categories:seo': [
					'warn',
					{ minScore: 1, aggregationMethod: 'pessimistic' },
				],
			},
		},
		upload: {
			target: 'temporary-public-storage',
		},
	},
};
