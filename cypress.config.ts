import { defineConfig } from 'cypress';

require('dotenv').config();

export default defineConfig({
	chromeWebSecurity: false,
	env: process.env,
	defaultCommandTimeout: 20000,
	e2e: {
		supportFile: 'cypress/support/command.cy.ts',
	},
});
